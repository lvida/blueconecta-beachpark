<?php

/*echo "<pre>";
print_r($_POST);*/

require_once('BeachParkClientWS.class.php');

$name = $_POST['inputName'];
$email= $_POST['inputEmail'];
$gender = $_POST['inputGender'];
$birthDate = $_POST['inputDay'].'/'.$_POST['inputMonth'].'/'.$_POST['inputYear'];
$dddTel = $_POST['inputDDDTel'];
$phone = $_POST['inputPhone'];
$dddCel = $_POST['inputDDDCel'];
$celPhone = $_POST['inputCelphone'];
$occupy = $_POST['inputOccupy'];
$marital_status = $_POST['inputMaritalStatus'];

$cadastro = Array(
    'name'=>$name,
    'email'=>$email,
    'gender'=>$gender,
    'birthDate'=>$birthDate,
    'dddTel'=>$dddTel,
    'phone'=>$phone,
    'dddCel'=>$dddCel,
    'celPhone'=>$celPhone,
    'occupy'=>$occupy,
    'maritalStatus'=>$marital_status
);

$beachParkDao = new BeachParkClientWS();
$result = $beachParkDao->insertFormBeachPark($cadastro);

if($result->status == 'success'){
    //echo "Sucesso";
    header("location:http://m.beachpark.com.br");
}else{
    //echo "Erro";
    header("location:javascript://history.go(-1)");
}
?>