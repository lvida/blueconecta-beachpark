<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="BlueConecta">
    <meta name="author" content="HDN Tecnologia">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-rim-auto-match" content="none">
    <title>Beach Park</title>
    <script src="js/jquery1-11.min.js"></script>
    <link rel="stylesheet" href="css/style.css<?php echo '?'.time();?>">
</head>

<?php
$status = 'D';
if(!isset($_SESSION))
    session_start();

$paths = $_SESSION['PATH_SYS'];
$pathsURL = $_SESSION['URL_SYS'];

?>


<body>

<script type="text/javascript">
    $(document).ready(function(){
        var height = $(document).height();
        var header = $("#btVoltar").height();
        var footer = $("#btNavegar").height();
        var content = $("#content").height() + 20; //20px of margins

        var totalBanner = height - (header + footer + content); //set the banner's image height in accord with the screen size

        $(".divBanner").css('height',totalBanner);
    });



</script>


<div id="containner">
    <div id="wrap" >
        <header>
            <div id="btVoltar">
                <a disabled id="back" href="javascript:history.back()""><span class="fa fa-angle-left"></span>< Voltar</a>
            </div>
        </header>
        <form action="execute_form.php" method="post">
                    <div class="bannerText">Para concorrer<br>basta preencher <br><br><br>Obrigado!</div>
                    <div class="divBanner"> <div style="background: url('enviar/imagens/logo_invertido2.png') no-repeat; z-index:9999;height: 75px;width: 200px;position: absolute;bottom:0px;right:0px;"></div></div>

                    <div id="content" >
                        <div class="box" style="overflow-y: auto;overflow-x: hidden;">
                            <div class="divPadding divName">
                                Nome:<br>
                                <input placeholder="Digite seu nome" type="text" name="inputName" id="inputName" class="inputProperties inputWidth inputName">
                            </div>

                            <div class="divPadding divEmail">
                                E-mail:<br>
                                <input placeholder="Digite seu e-mail" type="text" name="inputEmail" id="inputEmail" class="inputProperties inputWidth divEmail">
                            </div>

                            <div class="divGender">
                                Sexo:<br>
                                <select name="inputGender" class="inputProperties inputSelectBox">
                                    <option value="default" selected>Escolha seu sexo</option>
                                    <option value="male">Masculino</option>
                                    <option value="female">Feminino</option>
                                </select>
                            </div>

                            <div class="divPadding divAge">
                                Data de nascimento:<br>
                                <input placeholder="Dia" type="text" name="inputDay" class="inputProperties inputDay" ><input type="text" placeholder="Mês" name="inputMonth" class="inputProperties inputMonth"><input type="text" placeholder="Ano" name="inputYear" class="inputProperties inputYear">
                            </div>

                            <div class="divCelphone">
                                Celular:<br>
                                <input type="text" placeholder="DDD" name="inputDDDCel" class="inputProperties inputDDDCel"><input type="text" placeholder="Digite seu celular" name="inputCelphone"  class="inputProperties inputCelphone">
                            </div>

                            <div class="divPadding divPhone">
                                Telefone:<br>
                                <input type="text" placeholder="DDD" name="inputDDDTel" class="inputProperties inputDDDPhone"><input type="text" placeholder="Digite seu telefone" name="inputPhone"  class="inputProperties inputPhone">
                            </div>

                            <div class="divOccupy" >
                                Ocupação:<br>
                                <input placeholder="Digite sua ocupação" type="text" name="inputOccupy"  class="inputProperties inputOccupy">
                            </div>

                            <div class="divPadding divMaritalStatus">
                                Estado Civil:<br>
                                <select  name="inputMaritalStatus" class="inputProperties inputSelectBox">
                                    <option value="default" selected>Selecione</option>
                                    <option value="single">Solteiro</option>
                                    <option value="married">Casado</option>
                                    <option value="divorced">Divorciado</option>
                                    <option value="widower">Viúvo</option>
                                </select>
                            </div>
                        </div>
                        <p class="acessNet">* para acessar a internet,clique em navegar</p>
                        <div style="clear:both;"></div>
                    </div>
                <div class="clear"></div>
                <div id="placeHolderButtons">
                    <button type="submit" class="buttonSelect btNavegar btnCtrl" id="btNavegar" href="<?php echo $urlNavegar; ?>">Navegar na Internet</button>
                </div>
        </form>
    </div>
</div>
</body>
</html>
