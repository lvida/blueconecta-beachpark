<?php
/**
 * Created by PhpStorm.
 * User: Anderson
 * Date: 17/12/14
 * Time: 14:44
 */
ini_set("soap.wsdl_cache_enabled", 0);

 //define ('SOAP_URL','http://localhost/bservices/bservices/ClientWS/');
define ('SOAP_URL','http://bservices-dev.blueconecta.com.br/bservices/ClientWS/');


class SoapClientFactory {


    public static function externalServices(){
        return self::defaultSoapClient('ExternalServicesWS');
    }

    private static function defaultSoapClient($service){
        return new SoapClient(null, array(

           'uri' => SOAP_URL.$service.'/?wsdl',
           'location' => SOAP_URL.$service.'/index.php',
            //'uri' => SOAP_URL.'?wsdl',
            //'location' => SOAP_URL.'index.php',
            'trace' => true
        ));
    }




} 