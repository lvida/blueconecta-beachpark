<?php
/**
 * Created by PhpStorm.
 * User: OeMotenai
 * Date: 20/11/14
 * Time: 01:27
 */
//Index
define('BT_BACK','Back');
define('BT_CONNECT','Connect');
define('BT_SWIPE','Swipe');
define('BT_ENTER','Sign in');
define('BT_ANONNY','Or sign in without register');
define('BT_EMAILPLACELOGIN','Email Address');
define('LB_WELCOME','Welcome');
define('LB_GUEST','Guest');

//Contact
define('BT_TALK','Talk with us !');
define('BT_NAME','Name');
define('BT_NAMEPLACE','Your name');
define('BT_EMAIL','Email');
define('BT_EMAILPLACE','email@provider.com');
define('BT_MSG','Message');
define('BT_MSGPLACE','Write here...');
define('BT_SEND','Send');
define('MSG_SEND_EMAIL_SUCCESS','Message Successfully sent!');
define('MSG_SEND_EMAIL_FAIL','There was an error trying to send your message.');
define('MSG_SEND_ENTER_YOUR_NAME','Enter your name !');
define('MSG_SEND_ENTER_YOUR_MAIL','Enter your e-mail !');
define('MSG_SEND_ENTER_YOUR_MESSAGE','Enter your message !');

//Editar
define('BT_EDITTITLE','Edit user');
define('BT_GENDER','Gender');
define('BT_GENDERM','Male');
define('BT_GENDERF','Female');
define('BT_BIRTH','Date of Birth');
define('BT_LANGUAGE','Language');
define('BT_LANGUAGEP','Portuguese');
define('BT_LANGUAGEE','English');
define('BT_TERMS','Terms and conditions');
define('BT_TERMSACC','I read and accept the Terms and conditions');
define('BT_SAVEEDIT','Save changes');
define('MSG_EDIT_SUCCESS','Your changes have been saved.');
define('MSG_EDIT_FAIL','There was an error trying to change the register. Please try again a later');

//Register
define('BT_REGISTERTITLE','Sign Up');
define('BT_CONFIRMMAIL','Email Confirm');
define('BT_CONFIRMMAILPLACE','Confirm your Email');
define('BT_REGISTER','Register');
define('MSG_REGISTER_SUCCESS','Successful Sign Up.');
define('MSG_REGISTER_FAIL','There was an error trying to make the register. Please try again a later.');
define('MSG_DIFFERENT_EMAILS','E-mails are different !');
define('MSG_ENTER_EMAILS','Complete the email field !');
define('MSG_SELECT_GENDER','Select your Gender !');
define('MSG_ACCEPT_TERMS','You must accept the terms to complete the sign up !');

//Beon
define('MSG_ALTERNATIVE_EMAIL','Enter an alternate email');
define('MSG_SMS','SMS? To receive promotions');
define('MSG_INDICATE','Tell your friends');
define('MSG_INDICATE_TO','Enter the e-mail of your ');
define('MSG_FRIEND','friend');
define('MSG_READ_THE','Read the');
define('MSG_TERMS','terms of use');


//Beon Entrega
define('BT_NAVEGATION','Navigate the Internet');
define('BT_CONFIRM','Confirm');

//Geral
define('MSG_ALERT','Alert');

echo '<ul id="internationalization" style="display:none">';
echo '<li id="ITNback">'.BT_BACK.'</li>';
echo '<li id="ITNregistertitle">'.BT_REGISTERTITLE.'</li>';
echo '<li id="ITNedittitle">'.BT_EDITTITLE.'</li>';
echo '<li id="ITNconnect">'.BT_CONNECT.'</li>';
echo '<li id="ITNswipe">'.BT_SWIPE.'</li>';
echo '<li id="ITNenter">'.BT_ENTER.'</li>';
echo '<li id="ITNanonny">'.BT_ANONNY.'</li>';
echo '<li id="ITNemailplacelogin">'.BT_EMAILPLACELOGIN.'</li>';
echo '<li id="ITNwelcome">'.LB_WELCOME.'</li>';
echo '<li id="ITNguest">'.LB_GUEST.'</li>';
echo '<li id="ITNtalk">'.BT_TALK.'</li>';
echo '<li id="ITNname">'.BT_NAME.'</li>';
echo '<li id="ITNnameplace">'.BT_NAMEPLACE.'</li>';
echo '<li id="ITNemail">'.BT_EMAIL.'</li>';
echo '<li id="ITNemailplace">'.BT_EMAILPLACE.'</li>';
echo '<li id="ITNmsg">'.BT_MSG.'</li>';
echo '<li id="ITNmsgplace">'.BT_MSGPLACE.'</li>';
echo '<li id="ITNsend">'.BT_SEND.'</li>';
echo '<li id="ITNgender">'.BT_GENDER.'</li>';
echo '<li id="ITNgenderm">'.BT_GENDERM.'</li>';
echo '<li id="ITNgenderf">'.BT_GENDERF.'</li>';
echo '<li id="ITNbirth">'.BT_BIRTH.'</li>';
echo '<li id="ITNlanguage">'.BT_LANGUAGE.'</li>';
echo '<li id="ITNlanguagep">'.BT_LANGUAGEP.'</li>';
echo '<li id="ITNlanguagee">'.BT_LANGUAGEE.'</li>';
echo '<li id="ITNterms">'.BT_TERMS.'</li>';
echo '<li id="ITNtermsacc">'.BT_TERMSACC.'</li>';
echo '<li id="ITNsaveedit">'.BT_SAVEEDIT.'</li>';
echo '<li id="ITNconfirmmail">'.BT_CONFIRMMAIL.'</li>';
echo '<li id="ITNconfirmmailplace">'.BT_CONFIRMMAILPLACE.'</li>';
echo '<li id="ITNregister">'.BT_REGISTER.'</li>';
echo '<li id="ITNeditSuccess">'.MSG_EDIT_SUCCESS.'</li>';
echo '<li id="ITNeditFail">'.MSG_EDIT_FAIL.'</li>';
echo '<li id="ITNregisterSuccess">'.MSG_REGISTER_SUCCESS.'</li>';
echo '<li id="ITNregisterFail">'.MSG_REGISTER_FAIL.'</li>';
echo '<li id="ITNalert">'.MSG_ALERT.'</li>';
echo '<li id="ITNnavegation">'.BT_NAVEGATION.'</li>';
echo '<li id="ITNconfirm">'.BT_CONFIRM.'</li>';
echo '<li id="ITNalternativeEmail">'.MSG_ALTERNATIVE_EMAIL.'</li>';
echo '<li id="ITNsms">'.MSG_SMS.'</li>';
echo '<li id="ITNindicate">'.MSG_INDICATE.'</li>';
echo '<li id="ITNindicateToN">'.MSG_INDICATE_TO.'</li>';
echo '<li id="ITNFriend">'.MSG_FRIEND.'</li>';
echo '<li id="ITNreadThe">'.MSG_READ_THE.'</li>';
echo '<li id="ITNterms">'.MSG_TERMS.'</li>';
echo '<li id="ITNemailSuccess">'.MSG_SEND_EMAIL_SUCCESS.'</li>';
echo '<li id="ITNemailFail">'.MSG_SEND_EMAIL_FAIL.'</li>';
echo '<li id="ITNalertName">'.MSG_SEND_ENTER_YOUR_NAME.'</li>';
echo '<li id="ITNalertEmail">'.MSG_SEND_ENTER_YOUR_MAIL.'</li>';
echo '<li id="ITNalertMsg">'.MSG_SEND_ENTER_YOUR_MESSAGE.'</li>';
echo '<li id="ITNalertDifferentEmail">'.MSG_DIFFERENT_EMAILS.'</li>';
echo '<li id="ITNalertCompletEmail">'.MSG_ENTER_EMAILS.'</li>';
echo '<li id="ITNalertGender">'.MSG_SELECT_GENDER.'</li>';
echo '<li id="ITNalertAcceptTerms">'.MSG_ACCEPT_TERMS.'</li>';

echo'</ul>';