<?php
/**
 * Created by PhpStorm.
 * User: OeMotenai
 * Date: 20/11/14
 * Time: 01:27
 */
//Index
define('BT_BACK','Voltar');
define('BT_CONNECT','Conectar');
define('BT_SWIPE','Arrastar');
define('BT_ENTER','Entrar');
define('BT_ANONNY','Ou clique e navegue sem cadastrar');
define('BT_EMAILPLACELOGIN','Endereço de Email');
define('LB_WELCOME','Bem vind');
define('LB_GUEST','Visitante');

//Contact
define('BT_TALK','Fale conosco !');
define('BT_NAME','Name');
define('BT_NAMEPLACE','Seu nome');
define('BT_EMAIL','Email');
define('BT_EMAILPLACE','email@provedor.com');
define('BT_MSG','Mensagem');
define('BT_MSGPLACE','Escreva aqui...');
define('BT_SEND','Enviar');
define('MSG_SEND_EMAIL_SUCCESS','Mensagem Enviada com sucesso!');
define('MSG_SEND_EMAIL_FAIL','Houve um erro ao tentar enviar sua mensagem.');
define('MSG_SEND_ENTER_YOUR_NAME','Digite seu nome !');
define('MSG_SEND_ENTER_YOUR_MAIL','Preencha o campo de e-mail !');
define('MSG_SEND_ENTER_YOUR_MESSAGE','Escreva algo na mensagem !');


//Edit
define('BT_EDITTITLE','Editar Cadastro');
define('BT_GENDER','Sexo');
define('BT_GENDERM','Masculino');
define('BT_GENDERF','Feminino');
define('BT_BIRTH','Data de Nascimento');
define('BT_LANGUAGE','Idioma');
define('BT_LANGUAGEP','Português');
define('BT_LANGUAGEE','Inglês');
define('BT_TERMS','Termo de adesão');
define('BT_TERMSACC','Li e aceito o termo de adesão');
define('BT_SAVEEDIT','Salvar alterações');
define('MSG_EDIT_SUCCESS','Suas alterações foram salvas.');
define('MSG_EDIT_FAIL','Houve um erro ao tentar realizar seu cadastro. Tente novamente mais tarde.');

//Register
define('BT_REGISTERTITLE','Novo Usuário');
define('BT_CONFIRMMAIL','Confirmar Email');
define('BT_CONFIRMMAILPLACE','Confirme seu Email');
define('BT_REGISTER','Cadastrar');
define('MSG_REGISTER_SUCCESS','Cadastro realizado com sucesso.');
define('MSG_REGISTER_FAIL','Houve um erro ao tentar alterar seu cadastro. Tente novamente mais tarde.');
define('MSG_DIFFERENT_EMAILS','Os E-mails estão diferentes !');
define('MSG_ENTER_EMAILS','Preencha os campos de e-mail !');
define('MSG_SELECT_GENDER','Selecione seu Sexo !');
define('MSG_ACCEPT_TERMS','Você precisa aceitar os termos para efetuar o cadastro !');

//Beon
define('BT_NAVEGATION','Navegar na Internet');
define('BT_CONFIRM','Confirmar');

//Beon
define('MSG_ALTERNATIVE_EMAIL','Insira um e-mail alternativo');
define('MSG_SMS','SMS? Para receber promoções');
define('MSG_INDICATE','Indique para seus amigos');
define('MSG_INDICATE_TO','Informe o e-mail do seu ');
define('MSG_FRIEND','amigo');
define('MSG_READ_THE','Leia o ');
define('MSG_TERMS','termo de uso');

//Geral
define('MSG_ALERT','Alerta');

echo '<ul id="internationalization" style="display:none">';
echo '<li id="ITNback">'.BT_BACK.'</li>';
echo '<li id="ITNregistertitle">'.BT_REGISTERTITLE.'</li>';
echo '<li id="ITNedittitle">'.BT_EDITTITLE.'</li>';
echo '<li id="ITNconnect">'.BT_CONNECT.'</li>';
echo '<li id="ITNswipe">'.BT_SWIPE.'</li>';
echo '<li id="ITNenter">'.BT_ENTER.'</li>';
echo '<li id="ITNanonny">'.BT_ANONNY.'</li>';
echo '<li id="ITNemailplacelogin">'.BT_EMAILPLACELOGIN.'</li>';
echo '<li id="ITNwelcome">'.LB_WELCOME.'</li>';
echo '<li id="ITNguest">'.LB_GUEST.'</li>';
echo '<li id="ITNtalk">'.BT_TALK.'</li>';
echo '<li id="ITNname">'.BT_NAME.'</li>';
echo '<li id="ITNnameplace">'.BT_NAMEPLACE.'</li>';
echo '<li id="ITNemail">'.BT_EMAIL.'</li>';
echo '<li id="ITNemailplace">'.BT_EMAILPLACE.'</li>';
echo '<li id="ITNmsg">'.BT_MSG.'</li>';
echo '<li id="ITNmsgplace">'.BT_MSGPLACE.'</li>';
echo '<li id="ITNsend">'.BT_SEND.'</li>';
echo '<li id="ITNgender">'.BT_GENDER.'</li>';
echo '<li id="ITNgenderm">'.BT_GENDERM.'</li>';
echo '<li id="ITNgenderf">'.BT_GENDERF.'</li>';
echo '<li id="ITNbirth">'.BT_BIRTH.'</li>';
echo '<li id="ITNlanguage">'.BT_LANGUAGE.'</li>';
echo '<li id="ITNlanguagep">'.BT_LANGUAGEP.'</li>';
echo '<li id="ITNlanguagee">'.BT_LANGUAGEE.'</li>';
echo '<li id="ITNterms">'.BT_TERMS.'</li>';
echo '<li id="ITNtermsacc">'.BT_TERMSACC.'</li>';
echo '<li id="ITNsaveedit">'.BT_SAVEEDIT.'</li>';
echo '<li id="ITNconfirmmail">'.BT_CONFIRMMAIL.'</li>';
echo '<li id="ITNconfirmmailplace">'.BT_CONFIRMMAILPLACE.'</li>';
echo '<li id="ITNregister">'.BT_REGISTER.'</li>';
echo '<li id="ITNeditSuccess">'.MSG_EDIT_SUCCESS.'</li>';
echo '<li id="ITNeditFail">'.MSG_EDIT_FAIL.'</li>';
echo '<li id="ITNregisterSuccess">'.MSG_REGISTER_SUCCESS.'</li>';
echo '<li id="ITNregisterFail">'.MSG_REGISTER_FAIL.'</li>';
echo '<li id="ITNalert">'.MSG_ALERT.'</li>';
echo '<li id="ITNnavegation">'.BT_NAVEGATION.'</li>';
echo '<li id="ITNconfirm">'.BT_CONFIRM.'</li>';
echo '<li id="ITNalternativeEmail">'.MSG_ALTERNATIVE_EMAIL.'</li>';
echo '<li id="ITNsms">'.MSG_SMS.'</li>';
echo '<li id="ITNindicate">'.MSG_INDICATE.'</li>';
echo '<li id="ITNindicateToN">'.MSG_INDICATE_TO.'</li>';
echo '<li id="ITNFriend">'.MSG_FRIEND.'</li>';
echo '<li id="ITNreadThe">'.MSG_READ_THE.'</li>';
echo '<li id="ITNterms">'.MSG_TERMS.'</li>';
echo '<li id="ITNemailSuccess">'.MSG_SEND_EMAIL_SUCCESS.'</li>';
echo '<li id="ITNemailFail">'.MSG_SEND_EMAIL_FAIL.'</li>';
echo '<li id="ITNalertName">'.MSG_SEND_ENTER_YOUR_NAME.'</li>';
echo '<li id="ITNalertEmail">'.MSG_SEND_ENTER_YOUR_MAIL.'</li>';
echo '<li id="ITNalertMsg">'.MSG_SEND_ENTER_YOUR_MESSAGE.'</li>';
echo '<li id="ITNalertDifferentEmail">'.MSG_DIFFERENT_EMAILS.'</li>';
echo '<li id="ITNalertCompletEmail">'.MSG_ENTER_EMAILS.'</li>';
echo '<li id="ITNalertGender">'.MSG_SELECT_GENDER.'</li>';
echo '<li id="ITNalertAcceptTerms">'.MSG_ACCEPT_TERMS.'</li>';

echo'</ul>';



