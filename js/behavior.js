/**
 * Created by OeMotenai on 03/12/14.
 */
(function ($) {
    $(document).ready(function () {
        var element = document.getElementById('carousel');
        var carousel = new Carousel("#carousel");
        carousel.init();


        /*Internacionalization*/
        $("#back").text($("#ITNback").text());
        $(".btNavegar").text($("#ITNnavegation").text());
        $(".btnConfirmar").text($("#ITNconfirm").text());
        /*Internacionalization*/

        adjustSizes();

       // $('.fill').css('min-height', '100%').css('height', '+=10px');

       /* $('section').css('width', '100%').css('width', '-=10px');
        $('section').css('min-height', '100%').css('height', '-=10px');


        $('#carousel').css('width', '100%').css('width', '-=10px');

        $('.fill').css('min-height', '100%').css('height', '+=10px');*/
    });



    $(window).resize(function () {
        adjustSizes();

    });

    function adjustSizes()
    {
        $('ul #conteudo').css('height', '100%').css('height', '-=40px');

        $('section').css('width', '100%').css('width', '-=10px');
        $('section').css('height', '100%').css('height', '-=10px');


        $('#carousel').css('width', '100%').css('width', '-=10px');
        $('#carousel').css('height', $('section').height()+'px');


        $('.fill').css('height', (10+$('section').height())+'px');



        adjustImageWidth();
        adjustButtomHeight();
    }

    $(".btnConfirmar").click(function (e) {
        var count = 0;
        var parent = $(this).parent().parent();
        var campanhaAtiva = -1;
        var url = "./registraSessao.php";
        var hotspot = $("#hotspot").val();
        var user = $("#user").val();
        var opcoes = [];

        $(parent).find('input[type=checkbox]:checked').each(function () {
            count++;
            campanhaAtiva = $(this).attr('campanha');
            opcoes.push($(this).attr('id'));
        })

        if (count > 0) {
            e.preventDefault();
            var url = "./registraSessao.php";
            var data = {
                action: 1,
                campanhaAtiva: campanhaAtiva
            };
            var redirect = 'entregaCampanha.php';

            $.post(url, data, function (response) {
                if (redirect != '') {
                    registerParticipation(campanhaAtiva, opcoes, hotspot, user);
                    window.location.replace(redirect);
                }
            });
        } else {
            alert("Selecione ao menos uma opção.");
        }
    });

   /* $('.option').click(function (e) {
        e.preventDefault();
        var url = "./registraSessao.php";
        var hotspot = $("#hotspot").val();
        var user = $("#user").val();
        var campanha = $(this).attr('campanha');
        var opcoes = [];
        opcoes.push($(this).attr('id'));

        var data = {
            action: 1,
            campanhaAtiva: campanha
        };
        var redirect = $(this).attr('redirect');

        $.post(url, data, function (response) {
            if (redirect != '') {
               // registerParticipation(campanha, opcoes, hotspot, user);
            }
        });
    })*/

    function registerSession(campanha, opcoes, hotspot, usuario) {
        var url = "./registraSessao.php";

        var data = {
            action: 1,
            campanhaAtiva: campanha
        };

        $.post(url, data, function (response) {});
    }

    function registerParticipation(campanha, opcoes, hotspot, usuario) {
        var url = "./cadastraResposta.php";

        var data = {
            _campanha: campanha,
            _opcoes: opcoes,
            _hotspot: hotspot,
            _usuario: usuario
        }

        $.post(url, data, function (response) {

        });
    }

    function adjustImageWidth() {


        $('#carousel').find('.fill').each(function (e) {
            var count = 0;
            $(this).find('.btnCtrl').each(function (e) {
                count++;
            })
            if (count == 1) {
                $(this).find('.btnCtrl').each(function (e) {
                $(this).css('width', '100%');
                })
            }
        });
    }

    function adjustButtomHeight() {


        $('#carousel').find('.fill').each(function (e) {
            var count = 0;
            $(this).find('.backgroundText').each(function (e) {
                count++;
            })

            var totalHeight = $(window).height();
            var height = 0;

            var originalSize = $(this).children('#placeHolderImage').height();
            var buttomSize = 63;
            var margin = 5;
            var buttomNaveSize = $(this).children('#placeHolderButtons').children('#btNavegar').height();
            var finalSize = originalSize;

            if (count == 0) {
                finalSize = $(this).height() - buttomNaveSize - 16;
                height = totalHeight - $('header').height() - buttomNaveSize - 16;
            } else if (count > 0) {
                finalSize = $(this).height() - buttomNaveSize - margin - (buttomSize * count);
                height = totalHeight - $('header').height() - buttomNaveSize   - (buttomSize * count);
            }

            //alert(height);
            $(this).children('#placeHolderImage').height(height + 'px');
        });
    }

    function Carousel(element) {
        var self = this;
        element = $(element);

        var container = $(">ul", element);
        var panes = $(">ul>li", element);
        var pane_width = 0;
        var pane_count = panes.length;
        var current_pane = 0;


        /**
         * initial
         */
        this.init = function () {
            setPaneDimensions();
            $(window).on("load resize orientationchange", function () {
                setPaneDimensions();
            })
        };
        /**
         * set the pane dimensions and scale the container
         */
        function setPaneDimensions() {
            pane_width = element.width();
            panes.each(function () {
                $(this).width(pane_width);
            });
            container.width(pane_width * pane_count);
        };
        /**
         * show pane by index
         */
        this.showPane = function (index, animate) {
            // between the bounds
            index = Math.max(0, Math.min(index, pane_count - 1));
            current_pane = index;
            var offset = -((100 / pane_count) * current_pane);
            setContainerOffset(offset, animate);
        };
        function setContainerOffset(percent, animate) {
            container.removeClass("animate");
            if (animate) {
                container.addClass("animate");
            }
            if (Modernizr.csstransforms3d) {
                container.css("transform", "translate3d(" + percent + "%,0,0) scale3d(1,1,1)");
            }
            else if (Modernizr.csstransforms) {
                container.css("transform", "translate(" + percent + "%,0)");
            }
            else {
                var px = ((pane_width * pane_count) / 100) * percent;
                container.css("left", px + "px");
            }
        }

        this.next = function () {
            return this.showPane(current_pane + 1, true);
        };
        this.prev = function () {
            return this.showPane(current_pane - 1, true);
        };
        function handleHammer(ev) {

            // disable browser scrolling
            ev.gesture.preventDefault();
            var buttom = '';
            if ($(ev.srcElement).attr('id') == 'btNavegar') {
                buttom = ($(ev.srcElement));
                var url = $(buttom).attr('href');
                window.location.replace(url);
            }else if ($(ev.srcElement).hasClass('option') == true) {
                buttom = ($(ev.srcElement));
                var redirect = $(buttom).attr('redirect');


                var url = "./registraSessao.php";
                var hotspot = $("#hotspot").val();
                var user = $("#user").val();
                var campanha = $(buttom).attr('campanha');
                var opcoes = [];
                opcoes.push($(buttom).attr('id'));

                var data = {
                    action: 1,
                    campanhaAtiva: campanha
                };



                $.post(url, data, function (response) {
                    if (redirect != '') {
                        registerParticipation(campanha, opcoes, hotspot, user);
                        window.location.replace(redirect);
                    }
                });

                //window.location.replace(url);
            }
            else
                switch (ev.type) {
                    case 'dragright':
                    case 'dragleft':
                        // stick to the finger
                        var pane_offset = -(100 / pane_count) * current_pane;
                        var drag_offset = ((100 / pane_width) * ev.gesture.deltaX) / pane_count;
                        // slow down at the first and last pane
                        if ((current_pane == 0 && ev.gesture.direction == "right") ||
                            (current_pane == pane_count - 1 && ev.gesture.direction == "left")) {
                            drag_offset *= .4;
                        }
                        setContainerOffset(drag_offset + pane_offset);
                        break;
                    case 'swipeleft':
                        self.next();
                        $('#divSwipe').fadeOut(3000);
                        ;
                        ev.gesture.stopDetect();
                        break;
                    case 'swiperight':
                        self.prev();
                        ev.gesture.stopDetect();
                        break;
                    case 'release':
                        // more then 50% moved, navigate
                        if (Math.abs(ev.gesture.deltaX) > pane_width / 3) {
                            if (ev.gesture.direction == 'right') {
                                self.prev();
                            } else {
                                self.next();
                            }
                        }
                        else {
                            self.showPane(current_pane, true);
                        }
                        break;
                }
        }

        new Hammer(element[0], { dragLockToAxis: true }).on("release dragleft dragright swipeleft swiperight", handleHammer);
    }
})(jQuery);
