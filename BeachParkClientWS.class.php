<?php
/**
 * Created by PhpStorm.
 * User: admin_000
 * Date: 10/07/14
 * Time: 13:39
 */

define('BC_TOKEN', 'blueTeste');


//$paths = $_SESSION['PATH_SYS'];
require_once('SoapClientFactory.php');

class BeachParkClientWS {

    protected $client;

    function __construct() {
        $this->client = SoapClientFactory::externalServices();
    }


    public function insertFormBeachPark($campanha){

        $result = json_decode($this->client->insertFormBeachPark(BC_TOKEN, $campanha));

        return $result;

    }

}