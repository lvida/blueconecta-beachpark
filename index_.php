<?php

echo "<!DOCTYPE html>";

if (!isset($_SESSION)) {
    session_start();
}

//  require_once("../_configPaths.inc.php");
$paths = $_SESSION['PATH_SYS'];
$pathsUrl = $_SESSION['URL_SYS'];

require_once('/Idioma/pt_br.php');

//$idioma = 'pt_br';
if(isset($_SESSION['authLanguage']))
    $idioma = $_SESSION['authLanguage'];
if(isset($_SESSION['user']['language']))
    $idioma = $_SESSION['user']['language'];

?>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="BlueConecta">
    <meta name="author" content="HDN Tecnologia">
    <title>Beach Park</title>

    <link rel="stylesheet" href="css/style.css<?php echo '?'.time();?>">

    <script src="js/jquery.min.js"></script>
</head>
<body>
<!--    <?php //require_once($paths['idioma'].$idioma.'.php');?> -->
    <div id="containner">
        <input type="hidden" id="user" value="<?php echo $_SESSION['user']['userID']; ?>"/>
        <input type="hidden" id="hotspot" value="<?php echo $_SESSION['authIdHotspot']; ?>"/>
        <div id="wrap">
            <ul>
                <li>
                    <header>
                        <div id="btVoltar">
                            <a disabled id="back" href=<?php echo $_SESSION['URLHOME']; ?>"><span class="fa fa-angle-left"></span>< Voltar</a>
                        </div>
                    </header>
                </li>
                <li id="conteudo">
                    <section>
                        <div id="carousel">
                            <ul>
                                <li  class="fill" >
                                    <div id="placeHolderImage" style="background-color: #77CAEA;">
                                       <div class="divBanner"></div>
                                       <div class="bannerText">Para concorrer<br>basta preencher <br><br>Obrigado!</div>
                                        <form action="execute_form.php" method="post">
                                                <div class="box">
                                                    <div class="divPadding" style="margin-top:10px;">
                                                        Nome:<br>
                                                        <input placeholder="Digite seu nome" type="text" name="inputName" id="inputName" class="inputProperties inputWidth">
                                                    </div>
                                                    <div class="divPadding">
                                                        E-mail:<br>
                                                        <input placeholder="Digite seu e-mail" type="text" name="inputEmail" id="inputEmail" class="inputProperties inputWidth">
                                                    </div>
                                                    <div style="padding-left: 50%; position: absolute; width: 45%;">
                                                        Sexo:<br>
                                                        <select style="width: 100%; height: 24px;" name="inputGender" class="inputProperties">
                                                            <option value="default" disabled selected>Escolha seu sexo</option>
                                                            <option value="male">Masculino</option>
                                                            <option value="female">Feminino</option>
                                                        </select>
                                                    </div>
                                                    <div class="divPadding" style="width: 45%;">
                                                        Idade:<br>
                                                        <input placeholder="Dia" type="text" style="width: 18%;margin-right:10px;" name="inputDay" class="inputProperties" ><input type="text" placeholder="Mês" name="inputMonth" style="width: 18%;margin-right:10px;" class="inputProperties"><input type="text" placeholder="Ano" name="inputYear" style="width: 18%;" class="inputProperties">
                                                    </div>
                                                    <div class="divPadding" style="padding-left: 50%; position: absolute; width: 46%;">
                                                        Celular:<br>
                                                        <input type="text" placeholder="DDD" name="inputDDDCel" style="width: 18%;margin-right:10px;" class="inputProperties"><input type="text" placeholder="Digite seu celular" name="inputCelphone" style="width: 58%;" class="inputProperties">
                                                    </div>
                                                    <div class="divPadding" style="width: 45%;">
                                                        Telefone:<br>
                                                        <input type="text" placeholder="DDD" name="inputDDDTel" style="width: 18%; margin-right:10px;" class="inputProperties"><input type="text" placeholder="Digite seu telefone" name="inputPhone" style="width: 54%;" class="inputProperties">
                                                    </div>
                                                    <div style="padding-left: 50%; position: absolute; width: 42% " >
                                                        Ocupação:<br>
                                                        <input placeholder="Digite sua ocupação" type="text" name="inputOccupy" style="width: 100%;" class="inputProperties">
                                                    </div>
                                                    <div class="divPadding" style="width: 43%;">
                                                        Estado Civil:<br>
                                                        <select style="width: 100%; height: 24px;" name="inputMaritalStatus" class="inputProperties">
                                                            <option value="" disabled selected>Selecione</option>
                                                            <option value="single">Solteiro</option>
                                                            <option value="married">Casado</option>
                                                            <option value="divorced">Divorciado</option>
                                                            <option value="widower">Viúvo</option>
                                                        </select>
                                                    </div>

                                                </div>

                                        </div>
                                        <div class="clear"></div>
                                        <div id="placeHolderButtons">
                                            <button type="submit" class="buttonSelect btNavegar btnCtrl" id="btNavegar" href="<?php echo $urlNavegar; ?>">Navegar na Internet</button>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </section>
                </li>
            </ul>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
    </script>

<!--    <script src="js/modernizr.js"></script>
    <script src="js/hammer.js"></script>
    <script src="js/behavior.js"></script>-->
</body>
</html>